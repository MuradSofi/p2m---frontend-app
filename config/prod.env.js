'use strict'
const webpack = require('webpack')
const merge = require('webpack-merge')
const config = require('../webpack.config')

module.exports = merge(config, {
    plugins: [
        new webpack.DefinePlugin({
            'process.env': {
                BASE_API: "'http://10.10.10.63:8000/api/'",
            },
        }),
    ],
})