## Requirements
   P2M uses a number of ES6 features, so this project requires [Node.js](https://nodejs.org/) v6.0+ to be installed locally. A global install of VUE is also recommended.
   
   ## Installation
   To get the project up and running, and view components in the browser, complete the following steps:
   
   1. Download and install Node: <https://nodejs.org/>
   2. Clone this repo: `https://gitlab.com/MuradSofi/p2m---frontend-app.git` (HTTPS) 
   5. Install project dependancies: `yarn add`
   6. Start the development environment: `yarn dev`
   7. Open your browser
   
   
   
   ## Creating a static build
   To create a static instance of this project, run the following task:
   
   * `yarn build`
   
   
   ## Repo structure
   Sometimes it’s helpful to know what all these different files are for…
   
   ```
   /
   ├─ src/
   │  ├─ Assets/        # Assets
   │  │  ├─ Custom/     # Custom Assets File 
   │  │  ├─ Theme/      # Base Theme
   │  │
   │  ├─ components/    # Components
   │  │  ├─ Auth/  
   │  │  ├─ Common 
   │  │  ├─ Mediator 
   │  │  ├─ User 
   │  │  ├─ Utils 
   │  │
   │  ├─ Layouts/        # Layouts
   │  ├─ Middlewares/    # Middleware for check permission
   │  ├─ Models/         # Models for data format
   │  ├─ Pages/          # Application Pages
   │  ├─ Service/        # Services for standart requests
   │  config #For Change Request Ip
   │  
   └─ README.md         # This file
   ```