class Router {
    constructor(check) {
        if(check) return this;
        if(Router.instance) {
            return Router.instance;
        }else {
            Router.instance = new Router(true);
            Router.instance.routes = [];
            Router.instance.components = {};
            return Router.instance;
        }
    }

    create(path, component, middlewareList = []) {
        var filename =  component.__file.replace(/^.*[\\\/]/, '');
        var name = filename.substring(0, filename.lastIndexOf('.')) || filename;
        name = name.toLowerCase();
        var routerData = {
            path,
            component,
            meta: {
                middleware:middlewareList,
            }
        };
        Router.instance.routes.push(routerData);
        Router.instance.components[name+"-page"] = component;
        return this
    }

    addParams(obj) {
        Router.instance.routes[Router.instance.routes.length - 1].props = obj;
        return this;
    }
}
export default new Router();