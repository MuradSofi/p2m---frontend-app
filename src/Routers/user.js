import Router from "./index";

import Dashboard from "@Pages/User/Dashboard/Main"
import Profile from "@Pages/User/Profile/Main"
import CreateCase from "@Pages/User/Case/CreateCase/Main"
import SearchResult from "@Pages/User/SearchResult/Main"

import { auth } from "../Middlewares/auth";

Router.create("/user/", Dashboard, [auth]);
Router.create("/user/profile", Profile, [auth]);
Router.create("/user/create-case", CreateCase, [auth]);
Router.create("/user/search-result", SearchResult, [auth]);







