import Router from "./index";

import Dashboard from "@Pages/Mediator/Dashboard/Main"
import Profile from "@Pages/Mediator/Profile/Main"
import SearchResult from "@Pages/Mediator/SearchResult/Main"


import { auth } from "../Middlewares/auth";

Router.create("/mediator/", Dashboard, [auth]);
Router.create("/mediator/profile", Profile, [auth]);
Router.create("/mediator/public-cases", SearchResult, [auth]);







