import Vue from 'vue'
import VueRouter from 'vue-router'
import Router from "./Routers";
import App from './App.vue'


Vue.use(VueRouter);


require("./Routers/common");
require("./Routers/mediator");
require("./Routers/user");

const router = new VueRouter({
    mode: 'history',
    routes: Router.routes,
});


router.beforeEach((to, from, next) => {
    let {meta} = to;
    meta.hasOwnProperty("middleware") && (meta.middleware.every((node)=> node(to, router)) && next());
    require("./Assets/Theme/js/custom.js").init.call(window)
});

new Vue({
  el: '#app',
  render: h => h(App),
  router
});
