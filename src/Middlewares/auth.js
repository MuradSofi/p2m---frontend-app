export function auth(to, router) {
    const token = window.localStorage.getItem("token");
    return (token && true) || router.push('auth')
}

export function checkAuthPage(to, router) {
    const token = window.localStorage.getItem("token");
    return (!token && true) || router.push('/user')
}