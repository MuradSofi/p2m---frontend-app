export default class Main {
    constructor() {
        this.props = {
            title: {
                type: String,
                required: false
            },
            breadCrumbs: {
                type: Array,
                required: false
            }
        };
    }
}