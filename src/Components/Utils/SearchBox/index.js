import CommonService from "@Service/Common";

export default class Main {
    constructor() {
        this.props = {
            statistics: {
                type: Object,
                required: false
            }
        }
        this.data = () => ({
            categoryList: []
        })
        this.beforeMount = function () {
            CommonService.getCategoryList().then((response) => {
                this.categoryList = response
            });
        }
        this.updated = function () {
            $('.selectpicker').selectpicker('refresh');
        }
        this.watch = {
            statistics: {
                deep: true,
                handler() {
                    setTimeout(function () {
                        $('.counter-statistics').counterUp({
                            delay: 10,
                            time: 800
                        });
                    })
                }
            }
        }
    }
}