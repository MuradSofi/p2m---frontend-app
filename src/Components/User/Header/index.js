export default class Main {
    constructor() {
        this.methods = {
            logout() {
                window.localStorage.removeItem("token");
                this.$router.push("/auth")
            }
        }
    }
}