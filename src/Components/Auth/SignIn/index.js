import Auth from "@Service/Auth";
import { SIGNIN } from "@Models/Auth";
const SIGNINModel = new SIGNIN();
export default class Main {
    constructor() {
        this.data = () => ({
            formData: JSON.parse(JSON.stringify(SIGNINModel.formData)),
            status: JSON.parse(JSON.stringify( SIGNINModel.status)),
        });
        this.methods = {
            signUp() {
                this.$emit("changeType", "Sign-up")
            },
            signIn() {
                const {formData} = this;
                formData.submitDisabled = true;

                Auth.signIn(formData).then((response) => {
                    const { token, error } = response;
                    window.setTimeout(()=> {
                        formData.submitDisabled = false
                    }, 900);
                    error && (this.status.error = response.error);
                    if(token) {
                        localStorage.setItem("token", token)
                        const base64Url = token.split('.')[1];
                        const base64 = base64Url.replace('-', '+').replace('_', '/');
                        const userData = JSON.parse(window.atob(base64));
                        if(userData.user.role.label == "user") {
                            this.$router.push({path: "/user"})
                        }else {
                            this.$router.push({path: "/mediator"})
                        }
                    }
                    // token && ((localStorage.setItem("token", token) || this.$router.push({ path: '/' })))
                })
            }
        }
    }
}