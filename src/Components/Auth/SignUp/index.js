import Auth from "@Service/Auth";
import { SIGNUP } from "@Models/Auth";
const SIGNUPModel = new SIGNUP();
export default class Main {
    constructor() {
        this.data = () => ({
            formData: JSON.parse(JSON.stringify(SIGNUPModel.formData)),
            status: JSON.parse(JSON.stringify( SIGNUPModel.status)),
        });
        this.methods = {
            signIn() {
                this.$emit("changeType", "Sing-in")
            },
            signUp() {
                this.status = JSON.parse(JSON.stringify( SIGNUPModel.status));
                const {formData} = this;
                formData.submitDisabled = true;
                Auth.signUp(formData).then((response) => {
                    const { error, message } = response;
                    window.setTimeout(()=> {
                        formData.submitDisabled = false
                    }, 900);
                    (error && (this.status.error = response.error))
                    ||
                    (this.status.success = message, this.formData = JSON.parse(JSON.stringify(SIGNUPModel.formData)))
                })
            }
        }
    }
}