import MediatorService from "@Service/Mediator";
export default class Main {
    constructor() {
        this.props = {
            cases: {
                type: Array,
                required: false,
                default: []
            }
        }
        this.methods = {
            offer(id) {
                MediatorService.offer(id, {price: 0}).then((response) => {
                    console.log(response);
                })
            }
        }
    }
}