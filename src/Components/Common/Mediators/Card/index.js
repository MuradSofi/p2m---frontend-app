export default class Main {
    constructor() {
        this.props = {
            mediators: {
                type: Array,
                required: false,
                default: []
            }
        }
        this.updated = function () {
            $('.default-slick-carousel').slick("unslick");
            $('.default-slick-carousel').slick({
                infinite: false,
                slidesToShow: 3,
                slidesToScroll: 1,
                dots: false,
                arrows: true,
                adaptiveHeight: true,
                responsive: [
                    {
                        breakpoint: 1292,
                        settings: {
                            dots: true,
                            arrows: false
                        }
                    },
                    {
                        breakpoint: 993,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2,
                            dots: true,
                            arrows: false
                        }
                    },
                    {
                        breakpoint: 769,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            dots: true,
                            arrows: false
                        }
                    }
                ]
            });
        }
    }
}