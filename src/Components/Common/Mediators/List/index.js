export default class Main {
    constructor() {
        this.props = {
            mediators: {
                type: Array,
                required: false,
                default: []
            }
        }
    }
}