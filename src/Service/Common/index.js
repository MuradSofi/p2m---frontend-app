import API from "@Utils/ServiceAPI"
export default class Common {
    static getMediators() {
        const response = new API().get("mediators-top-list");
        return response;
    }

    static getCategoryList() {
        const response = new API().get("category-list");
        return response;
    }

    static getMediatorList() {
        const response = new API().get("mediator-list");
        return response;
    }

    static getStatisticsDashboard() {
        const response = new API().get("statistics-dashboard");
        return response;
    }
    static getStatistics() {
        const response = new API().get("statistics-user-mediator");
        return response;
    }
}