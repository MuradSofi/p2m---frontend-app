import API from "@Utils/ServiceAPI"

export default class Common {
    static getCases() {
        const response = new API().get("public-case");
        return response;
    }

    static offer(id, sendData) {
        const response = new API().request(sendData).post("offer-case", {id});
        return response;
    }

    static getDashboardStatistics()
    {
        const response = new API().get("statistics-user-mediator", {id});
        return response;
    }
}