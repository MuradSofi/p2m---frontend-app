import API from "@Utils/ServiceAPI"
export default class Auth {
    static signIn(formData) {
        const response = new API().request(formData).post("sign-in");
        return response;
    }
    static signUp(formData) {
        const response = new API().request(formData).post("sign-up");
        return response;
    }
}