import API from "@Utils/ServiceAPI"
export default class Auth {
    static openCaseList() {
        const response = new API().query({
            status: 0
        }).get("user-case-list");
        return response;
    }
    static closeCaseList() {
        const response = new API().query({
            status: 1
        }).get("user-case-list");
        return response;
    }

    static createCase(formData) {
        const response = new API().request(formData).post("create-case");
        return response;
    }

    static searchMediator(formData = {}) {
        let sendData = JSON.parse(JSON.stringify(formData));
        sendData.categories = sendData.categories.toString();
        const response = new API().query(sendData).get("mediator-list");
        return response;
    }
}