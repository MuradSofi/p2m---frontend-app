export class SIGNUP {
    constructor() {
        this.formData = {
            email: "",
            password: "",
            confirm_password: "",
            name: "",
            type: "user",
            submitDisabled: false
        }
        this.status = {
            error: "",
            success: ""
        }
    }
}

export class SIGNIN {
    constructor() {
        this.formData = {
            email: "",
            password: "",
            submitDisabled: false
        }
        this.status = {
            error: "",
        }
    }
}