import axios from "axios";
import qs from "qs";
import Exception from "@Utils/ExceptionHandler";

const ExceptionHandler = new Exception();

export default class Service {

    constructor() {
        const auth = require("@API/auth.json");
        const operator = require("@API/meditaor.json");
        const user = require("@API/user.json");
        const common = require("@API/common.json");
        this.urlList = Object.assign(auth, user, operator, common);
        this.requestBody = {};
        this.queryString = {};
        this.headerBody = {};
        this.method = null;
        this.response = {};
        this.url = '';
        this.urlParam = {};
    }
    request(obj) {
        this.requestBody = obj;
        return this;
    }

    header(obj) {
        this.headerBody = Object.assign(this.headerBody, obj);
        return this;
    }

    query(obj) {
        this.queryString = obj;
        return this;
    }

    get(url = '',urlParam = {}) {
        this.method = "get";
        this.urlParam = urlParam;
        this.url = url;
        return this.send()
    }

    post(url = '', urlParam = {}) {
        this.method = "post";
        this.urlParam = urlParam;
        this.url = url;
        return this.send()
    }

    put(url = '', urlParam = {}) {
        this.method = "post";
        this.urlParam = urlParam;
        this.headerBody = Object.assign(this.headerBody, {"X-HTTP-Method-Override": "PUT"});
        this.url = url;
        return this.send()
    }


    remove(url = '', urlParam = {}) {
        this.method = "delete";
        this.urlParam = urlParam;
        this.headerBody = Object.assign(this.headerBody, {"X-HTTP-Method-Override": "DELETE"});
        this.url = url;
        return this.send()
    }

    parseUrl(url){
        const regexp = /\$\{([a-zA-Z0-9_.]+)\}/g;
        return url.replace(regexp, (_, key) => this.urlParam[key]);
    }

    send() {
        let that = this;
        return new Promise(function (resolve) {
            let url = process.env.BASE_API+that.urlList[that.url];
            url = that.parseUrl(url);
            const token = window.localStorage.getItem("token");
            axios.defaults.headers.common['jwt-token'] = token ? token : "";
            axios({
                method: that.method,
                url: url +"?"+ qs.stringify(that.queryString),
                headers: that.headerBody,
                data: that.requestBody
            }).then((response)=>{
                that.response = response.data
            }).catch((error)=>{
                that.response = {
                    error: ExceptionHandler.handler(error)
                }
            }).then(()=>{
                resolve(that.response)
            });
        })
    }
}
