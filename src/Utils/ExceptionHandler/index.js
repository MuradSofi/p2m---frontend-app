export default class ExceptionHandler {
    constructor() {
        this.exceptions = {
            422: "VALIDATION_ERROR",
            400: "BAD_REQUEST",
            401: "UNAUTHORIZED"
        }
    }
    handler(error) {
        let { response, response: { status } } = error;
        return this[this.exceptions[status]](response)
    }
    VALIDATION_ERROR(error) {
        let errorMessage = "";
        for(let errorKey in error.data) {
            errorMessage += error.data[errorKey][0]
        }
        return errorMessage;
    }
    BAD_REQUEST(error) {
        let {data:{ error: errorMessage}} = error;
        return errorMessage;
    }
    UNAUTHORIZED(error) {
        window.localStorage.removeItem("token");
        window.location.href = "/auth";
    }
}

