import Layout from "@Layouts/User/Main";
import Header from "@Components/User/Header/Main";
import Footer from "@Components/User/Footer/Main";
import Sidebar from "@Components/Mediator/Sidebar/Main";

import SearchBox from "@Components/Utils/SearchBox/Main";
import Membership from "@Components/User/Membership/Main";

import CommonService from "@Service/Common";

export default class Main {
    constructor() {
        this.data = () => ({
            statistics: {
                closedDisputes: 0,
                offers: 0,
                openDisputes: 0
            }
        });

        this.components = {
            Layout,
            Header,
            Footer,
            Sidebar,
            SearchBox,
            Membership
        }
        this.beforeMount = function () {
            CommonService.getStatistics().then((response) => {
                this.statistics = response;
            })
        };

        this.watch = {
            statistics: {
                deep: true,
                handler() {
                    setTimeout(function () {
                        $('.fun-fact h4').counterUp({
                            delay: 10,
                            time: 800
                        });
                    })
                }
            }
        }
    }
}