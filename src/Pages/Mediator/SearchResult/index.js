import Layout from "../../../Layouts/Default/Main";
import Header from "../../../Components/Utils/Header/Main";
import Footer from "../../../Components/Utils/Footer/Main";

import SearchBox from "../../../Components/Utils/SearchBox/Main";
import CaseList from "../../../Components/Mediator/Cases/Main";

import {searchMediators} from "../../../Models/Common";
import CommonService from "@Service/Common";
import UserService from "@Service/User";
import MediatorService from "@Service/Mediator";

const SearchMediatorsModel = new searchMediators();

export default class Main {
    constructor() {
        this.data = () => ({
            cases: [],
            categoryList: [],
            searchFormData: JSON.parse(JSON.stringify(SearchMediatorsModel.searchFormData))
        })
        this.components = {
            Layout,
            Header,
            Footer,
            SearchBox,
            CaseList
        }
        this.beforeMount = function () {
            const vm = this;
            CommonService.getCategoryList().then(function (response) {
                vm.categoryList = response;
            })
            MediatorService.getCases().then((response) => {
                this.cases = response.data;
            })
        };
        this.updated = function () {
            $('.selectpicker').selectpicker('refresh');
        }
        this.methods = {
            search() {
                UserService.searchMediator(this.searchFormData).then((response) => {
                    this.cases = response.data
                })
            }
        }
    }
}