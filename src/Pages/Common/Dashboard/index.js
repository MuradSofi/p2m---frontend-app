import Layout from "@Layouts/Default/Main";
import Header from "@Components/Utils/Header/Main";
import Footer from "@Components/Utils/Footer/Main";

import SearchBox from "@Components/Utils/SearchBox/Main";
import Mediators from "@Components/Common/Mediators/Card/Main";

import CommonService from "@Service/Common"

export default class Main {
    constructor() {
        this.data = () => ({
            mediators: [],
            statistics: {
                closedDisputes: 0,
                mediators: 0,
                openDisputes: 0
            }
        });
        this.beforeMount = function () {
            CommonService.getMediators().then((response) => {
                this.mediators = response
            });
            CommonService.getStatisticsDashboard().then((response) => {
                this.statistics = response
            });
        };
        this.components = {
            Layout,
            Header,
            Footer,
            SearchBox,
            Mediators
        }
    }
}