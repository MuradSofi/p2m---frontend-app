import Layout from "@Layouts/Default/Main";
import Header from "@Components/Utils/Header/Main";
import Footer from "@Components/Utils/Footer/Main";

export default class Main {
    constructor() {
        this.components = {
            Layout,
            Header,
            Footer
        }
    }
}