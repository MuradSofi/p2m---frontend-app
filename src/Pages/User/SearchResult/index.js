import Layout from "../../../Layouts/Default/Main";
import Header from "../../../Components/Utils/Header/Main";
import Footer from "../../../Components/Utils/Footer/Main";

import SearchBox from "../../../Components/Utils/SearchBox/Main";
import Mediators from "../../../Components/Common/Mediators/List/Main";

import CommonService from "../../../Service/Common";
import UserService from "../../../Service/User";
import {searchMediators} from "../../../Models/Common";

const SearchMediatorsModel = new searchMediators();

export default class Main {
    constructor() {
        this.data = () => ({
            mediators: [],
            categoryList: [],
            searchFormData: JSON.parse(JSON.stringify(SearchMediatorsModel.searchFormData))
        })
        this.components = {
            Layout,
            Header,
            Footer,
            SearchBox,
            Mediators
        }
        this.beforeMount = function () {
            const vm = this;
            CommonService.getMediatorList().then(function (response) {
                vm.mediators = response.data;
            })
            CommonService.getCategoryList().then(function (response) {
                vm.categoryList = response;
                $('.selectpicker').selectpicker('refresh');
            })
        }
        this.updated = function () {
            $('.selectpicker').selectpicker('refresh');
        }
        this.methods = {
            search() {
                UserService.searchMediator(this.searchFormData).then((response) => {
                    this.mediators = response.data
                })
            }
        }
    }
}