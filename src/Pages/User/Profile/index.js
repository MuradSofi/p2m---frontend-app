import Layout from "@Layouts/User/Main";
import Header from "@Components/User/Header/Main";
import Footer from "@Components/User/Footer/Main";
import Sidebar from "@Components/Utils/Sidebar/Main";

import SearchBox from "@Components/Utils/SearchBox/Main";
import Membership from "@Components/User/Membership/Main";

import User from "@Service/User";

export default class Main {
    constructor() {
        this.data = () => ({
            openCases: [],
            closeCases: []
        });
        this.components = {
            Layout,
            Header,
            Footer,
            Sidebar,
            SearchBox,
            Membership
        }
        this.beforeMount = function() {
            User.closeCaseList().then((response) => {
                this.closeCases = response.data
            });
            User.openCaseList().then((response) => {
                this.openCases = response.data
            })
        }
    }
}