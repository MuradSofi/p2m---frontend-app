import Layout from "@Layouts/User/Main";
import Header from "@Components/User/Header/Main";
import Footer from "@Components/User/Footer/Main";
import Sidebar from "@Components/Utils/Sidebar/Main";

import SearchBox from "@Components/Utils/SearchBox/Main";
import Membership from "@Components/User/Membership/Main";

import {createCase} from "@Models/User"

import CommonService from "@Service/Common";
import UserService from "@Service/User";

const createCaseModel = new createCase();

import swal from "sweetalert2";

export default class Main {
    constructor() {
        this.data = () => ({
            categoryList: [],
            mediatorList: [],
            formData: JSON.parse(JSON.stringify(createCaseModel.formData))
        });
        this.components = {
            Layout,
            Header,
            Footer,
            Sidebar,
            SearchBox,
            Membership
        }
        this.beforeMount = function () {
            CommonService.getCategoryList().then((response) => {
                this.categoryList = response;
            });
            CommonService.getMediatorList().then((response) => {
                this.mediatorList = response.data;
            });
        };
        this.updated = function () {
            $('.selectpicker').selectpicker('refresh');
        }
        this.methods = {
            submitFormData() {
                let that = this;
                UserService.createCase(this.formData).then(function (response) {
                    let { error } = response;;
                    if(!error) {
                        const toast = swal.mixin({
                            toast: true,
                            position: 'top-end',
                            showConfirmButton: false,
                            timer: 3000
                        });
                        toast({
                            type: 'success',
                            title: response.message
                        })
                        that.formData = createCaseModel.formData;
                    }else {
                        const toast = swal.mixin({
                            toast: true,
                            position: 'top-end',
                            showConfirmButton: false,
                            timer: 3000
                        });
                        toast({
                            type: 'error',
                            title: error
                        })
                    }
                })
            }
        }
    }
}