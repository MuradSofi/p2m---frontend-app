import Layout from "@Layouts/Default/Main";
import Header from "@Components/Utils/Header/Main";
import Footer from "@Components/Utils/Footer/Main";
import Breadcrumb from "@Components/Utils/Breadcrumb/Main";


import SingIn from "@Components/Auth/SignIn/Main";
import SignUp from "@Components/Auth/SignUp/Main";

export default class Main {
    constructor() {
        this.components = {
            Layout,
            Header,
            Footer,
            Breadcrumb,
            SingIn,
            SignUp
        };
        this.data = () => (
            {
                bradCrumb: {
                    title: "Login",
                    breadCrumbs: [
                        {
                            name: "Home",
                            url: "/"
                        },
                        {
                            name: "Login",
                            url: "/auth"
                        }
                    ]
                },
                authType: "Sing-in"
            }
        );
        this.methods = {
            switchType(type) {
                this.authType = type;
            }
        }

    }
}